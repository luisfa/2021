
# REPOSITORIO FLISOL TENERIFE 2021
Este repositorio alberga toda la documentación del FLISoL Tenerife 2021

La organización está trabajando para estructurar y compartir todos los documentos en este repositorio gitlab.

## Cartel:   
El cartel es obra de Elena Salgado Mariñán, y es una obra derivada del cartel de 2019 realizado por Manz.  
![](https://gitlab.com/osl-ull/flisol-tenerife/2021/-/raw/master/Cartel/flisol_tenerife.png)

## Métodos de contacto:
+ Web: <https://flisol.info/FLISOL2021/Espana/Tenerife>
+ Correo: [flisoltenerife@disroot.org](mailto:flisoltenerife@disroot.org)
+ Mastodon: <https://txs.es/@flisoltenerife>
+ Peertube: <https://tuvideo.encanarias.info/accounts/flisoltenerife>
+ Archive.org: <https://archive.org/details/@flisoltenerife>
+ Twitter: <https://twitter.com/flisoltenerife>
+ Grupo Telegram: <https://t.me/flisoltenerife19/>

