# Programa preliminar del FLISoL-Tenerife 2021

## Sala General ([Sala 1]())

+ 9:00 GMT Bienvenida y Presentación

## [Sala 1]() Filosofía del Software Libre
+ 09:20
+ 09:40
+ 10:00
+ 10:20
+ 10:40
+ 11:00 Descanso 20 minutos
+ 11:20
+ 11:40
+ 12:00
+ 12:20
+ 12:40

## [Sala 2]() Aplicaciones y Talleres de Software Libre

+ 09:20
+ 09:40
+ 10:00
+ 10:20
+ 10:40
+ 11:00 Descanso 20 minutos
+ 11:20
+ 11:40
+ 12:00
+ 12:20
+ 12:40

## Sala general ([Sala 1]())

+ 13:00 GMT Mesa Redonda
