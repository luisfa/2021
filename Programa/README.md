# FLISOL TENERIFE 2021

## Repositorio del Programa del FLISoL-Tenerife 2021 y sus recursos

Este repositorio alberga la información del programa en sus distintos formatos. ¿pdf, odt, md, html, tex?.

En los directorios están las ponencias de cada sesión que de momento son dos y la de talleres si la hubiera:

+ Filosofía de Software Libre
+ Aplicaciones y Talleres de Software Libre


## Métodos de contacto:
+ Web: <https://flisol.info/FLISOL2021/Espana/Tenerife>
+ Grupo Telegram: <https://t.me/flisoltenerife19/>
+ [Correo](mailto://flisoltenerife@disroot.org): flisoltenerife@disroot.org
+ Redes Sociales: 
	+ Mastodon:
	+ [Twitter](https://twitter.com/flisoltenerife): https://twitter.com/flisoltenerife
	
	
