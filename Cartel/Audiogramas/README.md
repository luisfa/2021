# FLISOL TENERIFE 2021

## Repositorio de los Audiogramas.

Este repositorio alberga los recursos para la elaboración de audiogramas para el FLISoL_Tenerife_2021.

Se basa en el [cartel](https://gitlab.com/osl-ull/flisol-tenerife/2021/-/raw/master/Cartel/flisol_tenerife_basico.png) al que se le ha añadido un espacio para la foto perfilada sin fondo del ponente del audiograma en un círculo así como el audio del resumen elegido. Para obtener el video se recomienda usar las opciones del comando que se incluye como ejemplo, dónde audio.ogg es el fichero de audio e imagen.png el fichero de la imagen que se pondrá de fondo. El comando se ha pensado para trabajar en 1920x1080 de modo que la imagen debe tener esas dimensiones.

## Comando con **ffmpeg** Para realizar los audiogramas:
`ffmpeg -i audio.ogg -i imagen.png -filter_complex "[0:a]showwaves=s=1920x400:mode=cline:colors=orange[sw];[1][sw]overlay=x=W-w-0:y=800:format=auto,format=yuv420p[v]" -map "[v]" -map 0:a -movflags +faststart audiograma.mp4`


## Métodos de contacto:
+ Web: <https://flisol.info/FLISOL2021/Espana/Tenerife>
+ Correo: [flisoltenerife@disroot.org](mailto:flisoltenerife@disroot.org)
+ Mastodon: <https://txs.es/@flisoltenerife>
+ Peertube: <https://tuvideo.encanarias.info/accounts/flisoltenerife>
+ Archive.org: <https://archive.org/details/@flisoltenerife>
+ Twitter: <https://twitter.com/flisoltenerife>
+ Grupo Telegram: <https://t.me/flisoltenerife19/>

