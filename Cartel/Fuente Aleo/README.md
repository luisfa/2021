# Fuentes Libres para el FLISoL-Tenerife-2021

La [SIL Open Font License](SIL Open Font License.txt), Version 1.1, contiene el espíritu del desarrollo colaborativo de proyectos de fuentes de letras libres.

Para este FLISoL-Tenerife-2021, de momento se ha seleccionado la fuente Aleo de [Alesio Laiso](https://alessiolaiso.com/aleo-font). Se trata de una fuente contemporanea diseñada para acompañar la Lato Font de Lukasz Dziedzic, con detalles semi redodeados y estructura muy pulcra pone la legibilidad por delante pero alcanzando una fuerte personalidad en el diseño. Los seis estilos, tres pesos ligero, regular y negrita junto con una itálica real se encuentran en la carpeta como parte de los recursos. 
