# FLISOL TENERIFE 2021

## Repositorio del Cartel y los recursos

Este repositorio alberga todos los recursos para la elaboración del cartel del FLISoL_Tenerife_2021

### Carpetas: 

- [Fuente Aleo](Fuente Aleo/README.md)
- [Audiogramas](Audiogramas/README.md)


El [cartel](https://gitlab.com/osl-ull/flisol-tenerife/2021/-/raw/master/Cartel/flisol_tenerife_basico.png) es obra de Elena Salgado Mariñán, y es una obra derivada del [cartel de 2019](https://gitlab.com/osl-ull/flisol-tenerife/2019/-/blob/master/cartel/cartel/cartel-flisol-tenerife.png) realizado por Manz.

![a](flisol_tenerife.png)

Las fuentes se encuentran en la [carpeta Fuente Aleo](Fuente Aleo) y los recursos para los audiogramas en la [carpeta Audiogramas](Audiogramas).

## Métodos de contacto:
+ Web: <https://flisol.info/FLISOL2021/Espana/Tenerife>
+ Correo: [flisoltenerife@disroot.org](mailto:flisoltenerife@disroot.org)
+ Mastodon: <https://txs.es/@flisoltenerife>
+ Peertube: <https://tuvideo.encanarias.info/accounts/flisoltenerife>
+ Archive.org: <https://archive.org/details/@flisoltenerife>
+ Twitter: <https://twitter.com/flisoltenerife>
+ Grupo Telegram: <https://t.me/flisoltenerife19/>

